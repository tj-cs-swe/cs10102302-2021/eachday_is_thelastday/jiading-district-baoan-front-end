// pages/userAppointment/userAppointment.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        // state取值范围为： on, complete, canceled
         order_info: [],
         askCancel: false,
    },
  
    /**
     * 点击取消按钮
     * 
     */
    toCancel: function (event) {
        // 获取要取消订单的id
        let order_id = event.currentTarget.dataset.orderId;
        // 打开二次询问提示框
        this.setData({
            askCancel: true,
            // 在提示框的确认按钮里加上订单id
            op_order_id: order_id
        });
  
    },
  
    toFinish: function (event) {
      // 获取要完成订单的id
      let order_id = event.currentTarget.dataset.orderId;
      // 打开二次询问提示框
      this.setData({
          askCancel: true,
          // 在提示框的确认按钮里加上订单id
          op_order_id: order_id
      });

  },

  toFinish: function (event) {
    // 获取要完成订单的id
    let order_id = event.currentTarget.dataset.orderId;
    wx.request({
        url: 'https://garbage88classification.top/driver_complete?order_id='+ order_id,
    });
    wx.reLaunch({
        url:'../dirverAppointment/dirverAppointment?user_id=' + this.data.user_id,
      })
},

  toArrive: function (event) {
    // 获取要完成订单的id
    let order_id = event.currentTarget.dataset.orderId;
    wx.request({
        url: 'https://garbage88classification.top/driver_commit?order_id='+ order_id,
    });
    wx.reLaunch({
        url:'../dirverAppointment/dirverAppointment?user_id=' + this.data.user_id,
      })
},

  /**
   * 不取消订单，关闭二次询问提示框
   */
  noCancel: function () {
      this.setData({
          askCancel: false
      });
  },

  /*
   * 确认取消，向服务器发送请求
   */
  confirmCancel: function (event) {
      let that = this;
      // 被取消订单的id
      let canceled_order_id = event.currentTarget.dataset.orderId;
      // 被取消订单在order_info中的下标
      let index;
      for (index = 0; index < this.data.order_info.length; index++) {
          if (this.data.order_info[index].order_id === canceled_order_id) {
              break;
          }
      }
      // 更改订单状态
      this.setData({
          askCancel: false,
          [`order_info[${index}].state`]: 'canceled'
      });

      // 向服务器发送取消信息
      wx.request({
          url: 'https://garbage88classification.top/driver_cancel?order_id='+ canceled_order_id,
      });
      wx.reLaunch({
          url:'../dirverAppointment/dirverAppointment?user_id=' + this.data.user_id,
        })
  },
  
    toArrive: function (event) {
      // 获取要完成订单的id
      let order_id = event.currentTarget.dataset.orderId;
      wx.request({
          url: 'https://garbage88classification.top/driver_commit?order_id='+ order_id,
      });
      wx.reLaunch({
          url:'../dirverAppointment/dirverAppointment?user_id=' + this.data.user_id,
        })
  },
  
    /**
     * 不取消订单，关闭二次询问提示框
     */
    noCancel: function () {
        this.setData({
            askCancel: false
        });
    },
  
    /*
     * 确认取消，向服务器发送请求
     */
    confirmCancel: function (event) {
        let that = this;
        // 被取消订单的id
        let canceled_order_id = event.currentTarget.dataset.orderId;
        // 被取消订单在order_info中的下标
        let index;
        for (index = 0; index < this.data.order_info.length; index++) {
            if (this.data.order_info[index].order_id === canceled_order_id) {
                break;
            }
        }
        // 更改订单状态
        this.setData({
            askCancel: false,
            [`order_info[${index}].state`]: 'canceled'
        });
  
        // 向服务器发送取消信息
        wx.request({
            url: 'https://garbage88classification.top/driver_cancel?order_id='+ canceled_order_id,
        });
  
        // 显示取消成功的提示框
        {
            that.setData({
                succ_toast: true
            });
            setTimeout(() => {
                that.setData({
                    hide_succ_toast: true
                });
                setTimeout(() => {
                    that.setData({
                        succ_toast: false,
                        hide_succ_toast: false,
                    });
                }, 300);
            }, 2000);
        }
  
  
    },
    
    /**
     * 跳转到地图页面
     */
    toMap: function () {
        wx.navigateTo({
            url: '../map/map'
        });
    },
  
    toRoom: function(event){
        wx.navigateTo({
            url: '../room/room?order_id=' + event.currentTarget.dataset.orderId
        });
    },
  
    /**
      请求初始订单数据
     */
    onLoad: function (options) {
        let that = this;
        this.setData({
            user_id : options.user_id
        })
        wx.request({
            url: 'https://garbage88classification.top/diverReservations?user_id='+ this.data.user_id,
            method: 'POST',
            header: {
                "content-type": "application/x-www-form-urlencoded"
            },
            // 初始化订单数据
            success: function (res) {
                console.log(res);
                that.setData({
                    order_info: res.data.all_reservations
                });
            }
        });
  
    },
  
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
  
    },
  
    /**
     * 
     */
    onShow: function () {
  
    },
  
    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {
  
    },
  
    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {
  
    },
  
    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
  
    },
  
    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
  
    },
  
    onVtabBar_home:function(){
        wx.reLaunch({
          //url: 'camera/camera',
          url:'../dirverHomepage/dirverHomepage?user_id=' + this.data.user_id,
        })
    },
  
    onVtabBar_mine:function(){
        wx.reLaunch({
          //url: 'camera/camera',
          url:'../driverCenter/driverCenter?user_id=' + this.data.user_id,
          
        })
    },
  
    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {
        
    }
  
  
  })
  