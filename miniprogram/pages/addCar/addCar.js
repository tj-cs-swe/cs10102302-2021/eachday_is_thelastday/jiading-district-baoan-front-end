// miniprogram/pages/addCar/addCar.js
const SUCCESS_CODE = 1;
const LICENCE_ERROR = 2;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    brand:'',
    brandPhold:'输入车辆品牌',
    color:'',
    colorPhold:'输入车辆颜色',
    licence: '',
    licencePhold: '输入车牌号码',
    photo:'',
    carPhotoPhold:'上传车辆图片',

    brand_ok:false,  //合法性初步检查
    color_ok:false,  //合法性初步检查
    licence_ok:false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      this.setData({
        user_id: options.user_id
      })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  //获取输入的品牌,这里只给合法判断位赋值，无论合法与否都要setData
  onGetBrand:function(e){
    this.setData({ brand: e.detail.value });
    if(this.data.brand.length<=20 && this.data.brand.length>1){
      this.data.brand_ok=true
      console.log("品牌",this.data.brand)
    }
    else{
      this.data.brand_ok=false
    }
  },

  //获取输入的颜色,这里只给合法判断位赋值，无论合法与否都要setData
  onGetColor: function (e) {
    this.setData({ password: e.detail.value });
    this.data.color_ok=false;
    if(this.data.color.length<=20 && this.data.password.length>=1){
      this.data.color_ok=true;
      console.log("颜色",this.data.color)
    }
    else{
      this.data.color_ok=false;
    }
  },

  //获取输入的车牌号,这里只给合法判断位赋值，无论合法与否都要setData
onGetLicence: function (e) {
  this.setData({ licence: e.detail.value });
  this.data.licence_ok=false;
  if(this.data.licence.length<=10 && this.data.licence.length>=7){
    this.data.licence_ok=true;
    console.log("车牌号",this.data.licence)
  }
  else{
    this.data.licence_ok=false;
  }
},

  //获取输入的图片
  onBindCamera:function(){
    wx.navigateTo({
      //url: 'camera/camera',
      url:'../camera/camera',
    })
  },

/*  点击下拉框 */

bindShowMsg() {    this.setData({

  select: !this.data.select

})

},

  //确认添加
  onAddCar:function(e){
    console.log(this.data)
    // 经检验，确实赋值成功了，而且是.jpg的文件格式。说明我们的页面间跳转是成功的
    // console.log("====看看我们的变量carPhoto是不是赋值成功了====" + this.data.carPhoto)

    // 如果所有输入合法
    if(this.data.brand_ok && this.data.color_ok && this.data.licence_ok &&
      this.data.photo != '' ){
      let that = this;
      let upData=this.data;
      // //！！！！！！！！第二种上传方式（开始）
      wx.request({
        // url: 'https://garbage88classification.top/add_car',
        url: 'https://garbage88classification.top/add_car?user_id=' + this.data.user_id,
        data: upData,
        method: "POST",
        header: {
          // POST+这种content-type，会将数据转换成 query string
          "content-type": "application/x-www-form-urlencoded",
        },
        success: function (res) {
          console.log(res)
          // 显示注册成功的提示框并自动跳转到我的车辆页面
          if(res.data.status==SUCCESS_CODE){
            wx.navigateTo({
              url: '../my_cars/my_cars?user_id=' + res.data.user_id
            });
          }
          // 服务器通讯成功，但返回的结果是不通过：给出模态对话框提示
          else if(res.data.status==LICENCE_ERROR){
            wx.showModal({
              title: '提示',
              content: '此车牌号已被注册，请重新输入车牌号'
            })  
          }
        },
        fail: function (e) {
          // 这里是因为服务器通讯失败，用黑色提示框来显示
          console.log('wrong');
          console.log(e);
        }
      })
      // //！！！！！！！！第二种上传方式（结束）
    }

    else if(!this.data.brand_ok){
        wx.showModal({
          title: '提示',
          content: '请重新输入品牌'
        })
    }
    else if(!this.data.color_ok){
      wx.showModal({
        title: '提示',
        content: '请重新输入颜色'
      })  
    }
    else if(!this.data.licence_ok){
      wx.showModal({
        title: '提示',
        content: '请重新输入车牌号'
      })  
    } 
    else if(this.data.photo==''){
      wx.showModal({
        title: '提示',
        content: '请上传车辆图片！'
      })  
    }
  },
})
