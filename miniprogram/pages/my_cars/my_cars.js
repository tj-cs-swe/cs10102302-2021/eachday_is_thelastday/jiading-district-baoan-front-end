// pages/my_cars/my_cars.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        cars_info: {}

    },

    /**
     * 跳转到修改车辆信息页面
     * 被修改车辆的id以get方式通过url传递
     */
    modifyCarInfo: function (event) {
        wx.navigateTo({
            //url: 'modifyCarInfo?car_id=' + event.currentTarget.dataset.carId
            url:'../alterCar/alterCar?car_license=' + event.currentTarget.dataset.carId
        });
    },

    addNewCar: function () {
        wx.navigateTo({
            url: '../addCar/addCar?user_id=' + this.data.user_id
        });
    },


    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.setData({
            user_id: options.user_id
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     * 显示车辆信息
     */
    onShow: function () {
        let that = this;
        // 请求车辆数据
        wx.request({
            url: 'https://garbage88classification.top/requestCarInfo?user_id=' + this.data.user_id,
            header: {
                "content-type": "application/x-www-form-urlencoded"
            },
            method: "GET",
            //获取成功，更新车辆数据
            success: function (data) {
                console.log(data);
                that.setData({
                    cars_info: data.data.all_cars
                });
                console.log(that.data.cars_info);
                console.log(that.data.cars_info[0]);
            },
            // 获取失败，显示失败提示框
            fail: function (e) {
                that.setData({
                    warn_toast: true,
                });
                setTimeout(() => {
                    that.setData({
                        hide_warn_toast: true
                    });
                    setTimeout(() => {
                        that.setData({
                            warn_toast: false,
                            hide_warn_toast: false,
                        });
                    }, 300);
                }, 2000);
            }
        })
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})