// miniprogram/pages/register/register.js
// var http = require('../../utils/http.js')
const SUCCESS_CODE = 1;

const F_USERNAME_ILLEGAL=2;
const F_PASSWORD_ILLEGAL=3;

const F_USERNAME_REPETITION=4;
const F_ID_REPETITION=5;
const F_ID_INEXISTENCE=6;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    userName:'',
    userNamePhold:'用户名',
    password:'',
    passwordPhold:'密码',
    passwordAgain: '',
    passwordAgainPhold: '确认密码',
    photo:'',
    photo:'上传证件图片',
    id:'',
    idPhold:'学号/工号',
    phone:'',
    phonePhold:'手机号',

    userName_ok:false,  //合法性初步检查
    password_ok:false,  //合法性初步检查
    password_cfm_ok:false,
    id_ok:false,
    phone_ok:false,
    isUpload: false,

    warn_toast: false,
    hide_warn_toast: false,
    succ_toast: false,
    hide_succ_toast: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  //获取输入的用户名,这里只给合法判断位赋值，无论合法与否都要setData
  onGetUserName:function(e){
    this.setData({ userName: e.detail.value });
    if(this.data.userName.length<=20 && this.data.userName.length >= 5){
      this.data.userName_ok=true
      console.log("用户名",this.data.userName)
    }
    else{
      this.data.userName_ok=false
    }
  },

  //获取输入的密码,这里只给合法判断位赋值，无论合法与否都要setData
  onGetPassword: function (e) {
    this.setData({ password: e.detail.value });
    this.data.password_ok=false;
    if(this.data.password.length<=20 && this.data.password.length >= 6){
      this.data.password_ok=true;
      console.log("密码",this.data.password)
    }
    else{
      this.data.password_ok=false;
    }
  },

  //获取第二次输入的密码
  onGetPasswordAgain: function (e) {
    this.setData({ passwordAgain: e.detail.value });
    if(this.data.password==this.data.passwordAgain)
      this.data.password_cfm_ok=true;
  },
  
  //获取输入的图片
  onBindCamera:function(){
    wx.navigateTo({
      //url: 'camera/camera',
      url:'../../pages/camera/camera',
    })
  },

  //获取输入的学号
  onGetId: function (e){
    this.setData({ id: e.detail.value });
    if(this.data.id.length == 5 || this.data.id.length == 7){
      this.data.id_ok=true;
    }
    else{
      this.data.id_ok=false;
    }

  },

  //获取输入的手机号
  onGetPhone: function (e){
    this.setData({ phone: e.detail.value });
    if(this.data.phone.length == 11){
      this.data.phone_ok=true;
    }
    else{
      this.data.phone_ok=false;
    }
  },


  //注册
  onRegister:function(e){
    console.log(this.data)
    // 经检验，确实赋值成功了，而且是.jpg的文件格式。说明我们的页面间跳转是成功的
    // console.log("====看看我们的变量photo是不是赋值成功了====" + this.data.photoo)

    // 如果所有输入正确
    if(this.data.userName_ok && this.data.photo!='' && this.data.id_ok && 
      this.data.phone_ok && this.data.password_ok && this.data.password_cfm_ok ){
        
      // // 如果两次密码输入一致
      // if( this.password_ok ){

      let that = this;
      let upData=this.data;
      //开始检查用户名重复和学号重复的问题了

      // //！！！！！！！！第二种上传方式（开始）
      wx.request({
        // url: 'https://garbage88classification.top/passenger_signup',
        url: 'https://garbage88classification.top/passenger_signup',
        data: upData,
        method: "POST",
        header: {
          // POST+这种content-type，会将数据转换成 query string
          "content-type": "application/x-www-form-urlencoded",
        },
        success: function (res) {
          console.log(res)
          // 显示注册成功的提示框并自动跳转
          // 如果服务器返回的http状态码是SUCCESS_CODE,
          if (res.data==SUCCESS_CODE) {
              // 服务器通讯成功，并且返回的结果是通过：用黑色提示框来显示
              that.setData({
                succ_toast: true
              });
              setTimeout(() => {
                  that.setData({
                      hide_succ_toast: true
                  });
                  setTimeout(() => {
                      that.setData({
                          succ_toast: false,
                          hide_succ_toast: false,
                      });
                      // 自动跳转到登录          
                      wx.reLaunch({
                          url: '../userLogin/userLogin'
                      });
                  }, 300);
              }, 2000);


          }


          // 服务器通讯成功，但返回的结果是不通过：给出模态对话框提示
          else {
            switch(res.data){
              case F_USERNAME_ILLEGAL:
                wx.showModal({
                  title: '提示',
                  content: '用户名不合法，请重新输入'
                });
                break;
              case F_PASSWORD_ILLEGAL:
                wx.showModal({
                  title: '提示',
                  content: '密码不合法，请重新输入'
                });
                break;
              case F_USERNAME_REPETITION:
                wx.showModal({
                  title: '提示',
                  content: '该用户名已经被注册，请重新输入'
                });
                break;
              case F_ID_REPETITION:
                wx.showModal({
                  title: '提示',
                  content: '该学号/工号已注册，请直接登录'
                });
                break;
              case F_ID_INEXISTENCE:
                wx.showModal({
                  title: '提示',
                  content: '本校不存在该学号/工号，请重新输入'
                });
                break;
              default: break;

            }


          }
        },

        fail: function (e) {
          // 这里是因为服务器通讯失败，用黑色提示框来显示
          console.log('wrong');
          console.log(e);
          that.setData({
              warn_toast: true,
              msg: "注册失败：服务器通讯不通"
          });
          setTimeout(() => {
            that.setData({
                hide_warn_toast: true
            });
            setTimeout(() => {
                that.setData({
                    warn_toast: false,
                    hide_warn_toast: false,
                });
            }, 300);
          }, 2000);

        }


      })

      // //！！！！！！！！第二种上传方式（结束）

    }
    else if(!this.data.userName_ok){
        wx.showModal({
          title: '提示',
          content: '用户名长度不符（5-20字节）'
        })
    }
    else if(!this.data.password_ok){
      wx.showModal({
        title: '提示',
        content: '密码长度不符（6-20字节）'
      })  
    }
    else if(!this.data.password_cfm_ok){
      wx.showModal({
        title: '提示',
        content: '两次密码不匹配!'
      })  
    } 
    else if(this.data.photo==''){
      wx.showModal({
        title: '提示',
        content: '请上传证件图片！'
      })  
    }
    else if(!this.data.id_ok){
      wx.showModal({
        title: '提示',
        content: '请输入正确的学号/工号！'
      })  
    } 
    else if(!this.data.phone_ok){
      wx.showModal({
        title: '提示',
        content: '请输入正确的手机号！'
      })  
    } 
  },
})


