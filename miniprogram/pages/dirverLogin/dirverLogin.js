// pages/login2/login2.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        warn_toast: false,
        hide_warn_toast: false,
        succ_toast: false,
        hide_succ_toast: false,
        input_value: "" // 用于清空输入框
    },



    submitForm: function (event) {





        // 包含用户名和密码的表单数据对象
        let form_data = event.detail.value;

        // 用户名或密码为空，给出提示
        if (!(form_data.userName&&form_data.password)) {
            this.setData({
                warn_toast: true,
                msg:"输入错误"
            });
            setTimeout(() => {
                this.setData({
                    hide_warn_toast: true
                });
                setTimeout(() => {
                    this.setData({
                        warn_toast: false,
                        hide_warn_toast: false,
                    });
                }, 300);
            }, 2000);
        }
        // 发送登录请求
        else {
            let that = this;
            wx.request({
                // url: 'https://garbage88classification.top/login',
                url: 'https://garbage88classification.top/driver_login',
                header: {
                    "content-type": "application/x-www-form-urlencoded"
                },
                data: form_data,
                method: "POST",
                success: function (res) {
                    // 显示登录成功的提示框并自动跳转
                    if (res.data.status === 'ok') {
                        // 显示提示框
                        that.setData({
                            succ_toast: true
                        });
                        setTimeout(() => {
                            that.setData({
                                hide_succ_toast: true
                            });
                            setTimeout(() => {
                                that.setData({
                                    succ_toast: false,
                                    hide_succ_toast: false,
                                });
                                // 自动跳转到首页           
                                wx.reLaunch({
                                    url: '../dirverHomepage/dirverHomepage?user_id=' + res.data.user_id
                                });
                            }, 300);
                        }, 2000);
                    }
                    // 登录错误,给出提示,清空输入框
                    else {
                        that.setData({
                            warn_toast: true,
                            msg: "登录错误"
                        });
                        setTimeout(() => {
                            that.setData({
                                hide_warn_toast: true
                            });
                            setTimeout(() => {
                                that.setData({
                                    warn_toast: false,
                                    hide_warn_toast: false,
                                });
                            }, 300);
                        }, 2000);
                        // 清空输入框
                        that.setData({
                            input_value: ""
                        });
                    }
                },

                fail: function (e) {
                    console.log('wrong');
                    console.log(e);
                    that.setData({
                        warn_toast: true,
                        msg: "登录错误"
                    });
                    setTimeout(() => {
                        that.setData({
                            hide_warn_toast: true
                        });
                        setTimeout(() => {
                            that.setData({
                                warn_toast: false,
                                hide_warn_toast: false,
                            });
                        }, 300);
                    }, 2000);
                }
            })
        }
    },




    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
    
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     * 
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})