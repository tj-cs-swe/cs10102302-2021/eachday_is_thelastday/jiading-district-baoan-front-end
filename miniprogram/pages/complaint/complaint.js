// miniprogram/pages/complaint/complaint.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    // var passenger_id = 1;
    var app = getApp()
    console.log(app.globalData.USERNAME_)
    
    wx.request({
      // url: 'https://garbage88classification.top/requestOrderListInfo?passenger_id=' + passenger_id,
      url: 'https://garbage88classification.top/requestOrderListInfo',
      data: app.globalData.USERNAME_,
      header: {
        // POST+这种content-type，会将数据转换成 query string
        "content-type": "application/x-www-form-urlencoded",
      },
      method: "GET",
      success: function (res) {
        that.setData({
          list: res.data,
        })
        
      },
        
    })


  },

  complainThisOrder: function (event) {
    wx.navigateTo({
      // url: 'modifyCarInfo?car_id=' + event.currentTarget.dataset.carId
      // 传参的时候把订单号传过去
      url:'../inputComplaint/inputComplaint?order_id='+ '',
    });


  },

  






  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})