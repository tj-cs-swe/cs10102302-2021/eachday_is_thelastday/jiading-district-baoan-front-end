// miniprogram/pages/homepage/homepage.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    items: [],
    count: 0,
    isHideLoadMore: true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      this.setData({
        user_id: options.user_id
      })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    // this.onGetActivities();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var that = this;
    wx.showNavigationBarLoading(); //在标题栏中显示加载            
    setTimeout(function () {
      // complete 
      that.onGetActivities();
      wx.hideNavigationBarLoading(); //完成停止加载
      wx.stopPullDownRefresh(); //停止下拉刷新      
    }, 1500);
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  onVtabBar_mine:function(){
    wx.reLaunch({
      //url: 'camera/camera',
      url:'../userCenter/userCenter?user_id=' + this.data.user_id,
    })
  },

  to_frequentOrder:function(){
    wx.navigateTo({
      //url: 'camera/camera',
      url:'../frequentOrder/frequentOrder?user_id=' + this.data.user_id,
    })
  },

  onVtabBar_Appointment:function(){
    wx.reLaunch({
      //url: 'camera/camera',
      url:'../myAppointment/myAppointment?user_id=' + this.data.user_id,
    })
  },

  onReserve:function(){

    
  }



})