// miniprogram/pages/inputComplaint/inputComplaint.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    myComplaint:'',
    myComplaintPhold:'请在此输入评价',

    myComplaint_ok:false,

    warn_toast: false,
    hide_warn_toast: false,
    succ_toast: false,
    hide_succ_toast: false,


  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      this.setData({
        order_id: options.order_id
      })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  //获取输入的订单评价,这里只给合法判断位赋值，无论合法与否都要setData
  onGetMyComplaint:function(e){
    this.setData({ myComplaint: e.detail.value });
    if(this.data.myComplaint.length<=100 && this.data.myComplaint.length>4){
      this.data.myComplaint_ok=true
      console.log("订单投诉反馈",this.data.myComplaint)
    }
    else{
      this.data.myComplaint_ok=false
    }
  },


  complainThisOrder:function(e){
    console.log(this.data)

    // 如果输入合法
    if(this.data.myComplaint_ok){
      let that = this;
      let upData=this.data;

      wx.request({
        // url: 'https://garbage88classification.top/driver_signup',
        url: 'https://garbage88classification.top/passenger_comment?order_id=' + this.data.order_id,
        data: upData,
        method: "POST",
        header: {
          // POST+这种content-type，会将数据转换成 query string
          "content-type": "application/x-www-form-urlencoded",
        },
        success: function (res){
          console.log(res)

          if(true){
          //if (res.data==SUCCESS_CODE) {

            // 服务器通讯成功，并且返回的结果是通过：用黑色提示框来显示
            that.setData({
              succ_toast: true
            });
            setTimeout(() => {
                that.setData({
                    hide_succ_toast: true
                });
                setTimeout(() => {
                    that.setData({
                        succ_toast: false,
                        hide_succ_toast: false,
                    });
                    // 自动跳转回到订单评价页面           
                    wx.reLaunch({
                        url: '../myAppointment/myAppointment?user_id=' + res.data.user_id
                    });
                }, 300);
            }, 2000);

          }

        },

        fail: function (e) {
          // 这里是因为服务器通讯失败，用黑色提示框来显示
          console.log('wrong');
          console.log(e);
          that.setData({
              warn_toast: true,
              msg: "提交评价失败：服务器通讯不通"
          });
          setTimeout(() => {
            that.setData({
                hide_warn_toast: true
            });
            setTimeout(() => {
                that.setData({
                    warn_toast: false,
                    hide_warn_toast: false,
                });
            }, 300);
          }, 2000);
        }


      })

    }

    else{
      wx.showModal({
        title: '提示',
        content: '投诉长度不符（5-100字节）'
      })
    }


  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})