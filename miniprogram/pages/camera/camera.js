// pages/camera/camera.js
// var md5 = require('../../utils/md5.js')
var http = require('../../utils/http.js')
// var util = require('../../utils/util.js')
// import { Utilaa } from 'util'
// var u = require('underscore')
Page({
  data: {
    accessToken: "",
    isUpload: false,
    src: "",
    isCamera: true,
    btnTxt: "拍照"
  },

  onLoad() {
    // ctx 实际上是通过 wx.createCameraContext 获取的CameraContext 实例
    this.ctx = wx.createCameraContext()
    var time = wx.getStorageSync("time")
    var curTime = new Date().getTime()
    var timeInt = parseInt(time)
    var timeNum = parseInt((curTime - timeInt) / (1000 * 60 * 60 * 24))
    console.log("====timeNum是====" + timeNum)
    // 从本地缓存中同步获取指定 key 的内容，这里的key是access_token
    var accessToken = "24.ae8347e57ffadf13bfecc0617b353a04.2592000.1618299303.282335-23794064"//wx.getStorageSync("access_token")
    console.log("====accessToken是====" + accessToken + "a")

    if (timeNum > 28 || (accessToken == "" ||
      accessToken == null || accessToken == undefined)) {
        console.log("这是timeNum>28或accessToken无定义的情况")

        this.takePhoto()
        console.log("takephoto可以用")
      } 
    else {
      console.log("这是timeNum<28且accessToken有定义的情况")
      this.setData({
        accessToken: wx.getStorageSync("access_token")
      })
    }

  },


  takePhoto() {
    console.log("这里是takephoto功能")
    var that = this
    if (this.data.isCamera == false) {
      this.setData({
        isCamera: true,
        btnTxt: "拍照"
      })
      return
    }
    // 实际上是CameraContext.takePhoto()，用于拍摄图片
    this.ctx.takePhoto({
      quality: 'low',
      success: (res) => {
        // this.ctx.takePhoto成功之后，就是拍完了，
        // 拍下来的照片存在临时路径 res.tempImagePath里面,res.tempImagePath是一串字符，输出发现结尾是.jpg
        this.setData({
          src: res.tempImagePath,
          isCamera: false,
          btnTxt: "重拍"
        })
        console.log("临时路径tempImagePath为:");
        console.log(res.tempImagePath);
        wx.showLoading({
          title: '正在加载中',
        })

        wx.getFileSystemManager().readFile({
          // res.tempImagePath 是刚刚拍下来的照片在手机内的的临时路径，输出发现结尾是.jpg
          filePath: res.tempImagePath,
          encoding: "base64",
          success: res => {
            // wx.getFileSystemManager().readFile成功后， res.data就是文件内容

            // 所以 res.data 就是你从文件读出来的内容，即你拍的那张图片！类型是string/ArrayBuffer
            // that.req("24.ae8347e57ffadf13bfecc0617b353a04.2592000.1618299303.282335-23794064", res.data)

            console.log("看一下从文件中读出的res.data：")
            console.log(res.data)
            var pages = getCurrentPages();
            var prevPage = pages[pages.length - 2];  //上一个页面
            //直接调用上一个页面的setData()方法，把数据存到上一个页面（register）中去
            prevPage.setData({ photo: res.data })
            wx.showToast({
              title: '拍照成功',
              icon: "none"
            })

            //2秒之后跳转回到register页面
            setTimeout(function () {
              wx.navigateBack({
                delta: 1
              })
            }, 2000);

          },
          fail: res => {
            wx.hideLoading()
            wx.showToast({
              title: '拍照失败,未获取相机权限或其他原因',
              icon: "none"
            })
          }
        })


      }
    })
  },

  error(e) {
    console.log(e.detail)
  }


  

})