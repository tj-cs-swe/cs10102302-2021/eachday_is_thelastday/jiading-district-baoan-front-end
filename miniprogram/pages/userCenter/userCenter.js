// pages/userCenter/userCenter.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
      passenger_info: {}
  },


  queryMyCar: function () {
      wx.navigateTo({
          //-url: 'queryMyCar?driver_id=' + driver_id
          url: '../my_cars/my_cars'
      });
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
        this.setData({
            user_id: options.user_id
        })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
      let that = this;
      // 请求乘客信息
      wx.request({
          url: 'https://garbage88classification.top/requestPassengerInfo?user_id=' + this.data.user_id,
          header: {
              "content-type": "application/x-www-form-urlencoded"
          },
          method: "GET",
          //获取成功，更新司机信息
          success: function (data) {
              that.setData({
                  passenger_info: data.data
              });
          },
          // 获取失败，显示失败提示框
          fail: function (e) {
              that.setData({
                  warn_toast: true,
              });
              setTimeout(() => {
                  that.setData({
                      hide_warn_toast: true
                  });
                  setTimeout(() => {
                      that.setData({
                          warn_toast: false,
                          hide_warn_toast: false,
                      });
                  }, 300);
              }, 2000);
          }
      })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  onVtabBar_Appointment:function(){
    wx.reLaunch({
      //url: 'camera/camera',
      url:'../myAppointment/myAppointment?user_id=' + this.data.user_id,
    })
  },

  onVtabBar_homepage:function(){
    wx.reLaunch({
      //url: 'camera/camera',
      url:'../userHomepage/userHomepage?user_id=' + this.data.user_id,
    })
  }
})

