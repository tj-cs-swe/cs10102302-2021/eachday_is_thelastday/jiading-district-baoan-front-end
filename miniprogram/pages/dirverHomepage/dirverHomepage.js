// miniprogram/pages/dirverHomepage/dirverHomepage.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    items: [],
    count: 0,
    isHideLoadMore: true,
    whether_leisure: 0,
    status_css: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this;
    this.setData({
        user_id : options.user_id
    })
    wx.request({
        url: 'https://garbage88classification.top/driver_comment?user_id='+ this.data.user_id,
        method: 'POST',
        header: {
            "content-type": "application/x-www-form-urlencoded"
        },
        // 初始化订单数据
        success: function (res) {
            console.log(res);
            that.setData({
                items: res.data.all_marks_comments,
                whether_leisure: res.data.driver_status
            });
            console.log(that.data.whether_leisure);
            let ch = '开始接单';
            if(that.data.whether_leisure == 1){
              ch = '停止接单';
            };
            that.setData({
              status_css: ch
            });
        }
    });
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    // this.onGetActivities();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var that = this;
    wx.showNavigationBarLoading(); //在标题栏中显示加载            
    setTimeout(function () {
      // complete 
      that.onGetActivities();
      wx.hideNavigationBarLoading(); //完成停止加载
      wx.stopPullDownRefresh(); //停止下拉刷新      
    }, 1500);
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  onVtabBar_mine:function(){
    wx.reLaunch({
      //url: 'camera/camera',
      url:'../driverCenter/driverCenter?user_id=' + this.data.user_id,
    })
  },

  onVtabBar_Appointment:function(){
    wx.reLaunch({
      //url: 'camera/camera',
      url:'../dirverAppointment/dirverAppointment?user_id=' + this.data.user_id,
    })
  },

  to_myCars:function(){
    wx.navigateTo({
      //url: 'camera/camera',
      url:'../my_cars/my_cars?user_id=' + this.data.user_id,
    })
  },


    /**
   * 暂停、开始接单
   */
  onLeisure: function() {
    let page = this;
    if (page.data.whether_leisure == 1){
      page.setData({
        status_css: '开始接单',
        whether_leisure: 0,
      });
      
      wx.request({
        url: 'https://garbage88classification.top/driver_logout?user_id='+ this.data.user_id,
      });
    }
    else{
      page.setData({
        status_css: '停止接单',
        whether_leisure: 1,
      });
      wx.request({
        url: 'https://garbage88classification.top/driver_accept?user_id='+ this.data.user_id,
      });
    }
  },

  onReserve:function(){

    
  },




})