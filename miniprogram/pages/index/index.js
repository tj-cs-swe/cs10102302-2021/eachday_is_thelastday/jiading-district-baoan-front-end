// pages/index/index.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        pictures: ["img/1.jpg", "img/2.jpg", "img/3.jpg"],
        interval: 3000,
        loading: false,
        hide_loading: false,
        warn_toast: false,
        hide_warn_toast: false

    },

    /**
     * 跳转到登录或者注册页面
     */
    redirect: function (event) {
        switch (event.currentTarget.dataset.choice) {
            case "login1":
                wx.navigateTo({
                    url: '../userLogin/userLogin'
                });
                break;
                case "login2":
                    wx.navigateTo({
                        url: '../dirverLogin/dirverLogin'
                    });
                    break;
                case "passenger_register":
                wx.navigateTo({
                    url: '../userRegister/userRegister'
                });
                break;
            case "driver_register":
                wx.navigateTo({
                    url: '../dirverRegister/dirverRegister'
                });
                break;
        }
    },


    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     * 与后端进行连接确认
     */
    onReady: function () {
        // 显示连接提示框
        this.setData({
            loading: true,
            hide_loading: true

        });
        let that = this;
        // 请求连接
        wx.request({
            url: 'https://garbage88classification.top/',
            header: {
                "content-type": "application/x-www-form-urlencoded"
            },
            data: "request connection",
            method: "POST",
            // 连接到服务器 关闭连接提示框
            success: function () {
                that.setData({
                    loading: false,
                    hide_loading: false
                });
            },
            // 连接失败  关闭连接提示框, 显示失败提示框
            fail: function (e) {
                that.setData({
                    warn_toast: true,
                    loading: false,
                    hide_loading: false
                });
                setTimeout(() => {
                    that.setData({
                        hide_warn_toast: true
                    });
                    setTimeout(() => {
                        that.setData({
                            warn_toast: false,
                            hide_warn_toast: false,
                        });
                    }, 300);
                }, 2000);
            }
        })







    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})