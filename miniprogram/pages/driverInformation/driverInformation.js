// miniprogram/pages/driverInformation/driverInformation.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    driver_name: '1',
    driver_phone: '2',
    car_brand: '3',
    car_color: '4',
    car_licence: '5'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options);
    this.setData({
      driver_name: options.driver_name,
      driver_phone: options.driver_phone,
      car_brand: options.car_brand,
      car_color: options.car_color,
      car_licence: options.car_licence
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})