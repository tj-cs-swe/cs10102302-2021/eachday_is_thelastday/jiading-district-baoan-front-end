// miniprogram/pages/addCar/addCar.js
const SUCCESS_CODE = 1;

const F_BRAND = 2;
const F_COLOR = 3;

const F_PLATE_NUMBER = 4;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    brand:'',
    brandPhold:'原品牌',
    color:'',
    colorPhold:'原颜色',
    plateNumber: '',
    plateNumberPhold: '原车牌号',
    carPhoto:'',
    carPhotoPhold:'原车辆图片',

    brand_ok:true,  //合法性初步检查
    color_ok:true,  //合法性初步检查
    plateNumber_ok:true,
    isUpload: false,

    warn_toast: false,
    hide_warn_toast: false,
    succ_toast: false,
    hide_succ_toast: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      this.setData({
        car_license: options.car_license
      })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  //获取输入的品牌,这里只给合法判断位赋值，无论合法与否都要setData
  onGetBrand:function(e){
    this.setData({ brand: e.detail.value });
    if(this.data.brand.length<=20 && this.data.brand.length>1){
      this.data.brand_ok=true
      console.log("品牌",this.data.brand)
    }
    else{
      this.data.brand_ok=false
    }
  },

  //获取输入的颜色,这里只给合法判断位赋值，无论合法与否都要setData
  onGetColor: function (e) {
    this.setData({ color: e.detail.value });
    if(this.data.color.length<=20 && this.data.password.length>=1){
      this.data.color_ok=true;
      console.log("颜色",this.data.color)
    }
    else{
      this.data.color_ok=false;
    }
  },

  //获取输入的车牌号,这里只给合法判断位赋值，无论合法与否都要setData
onGetPlateNumber: function (e) {
  this.setData({ plateNumber: e.detail.value });
  if(this.data.plateNumber.length<=10 && this.data.plateNumber.length>=7){
    this.data.plateNumber_ok=true;
    console.log("车牌号",this.data.plateNumber)
  }
  else{
    this.data.plateNumber_ok=false;
  }
},

  //获取输入的图片
  onBindCamera:function(){
    wx.navigateTo({
      //url: 'camera/camera',
      url:'../../pages/camera/camera',
    })
  },

  //确认添加
  onAddCar:function(e){
    console.log(this.data)
    // 经检验，确实赋值成功了，而且是.jpg的文件格式。说明我们的页面间跳转是成功的
    // console.log("====看看我们的变量carPhoto是不是赋值成功了====" + this.data.carPhoto)

    // 如果所有输入合法
    if(this.data.brand_ok && this.data.color_ok && this.data.plateNumber_ok &&
      this.data.photo != '' ){
      let that = this;
      let upData=this.data;

      // //！！！！！！！！第二种上传方式（开始）
      wx.request({
        // url: 'https://garbage88classification.top/driver_signup',
        url: 'https://garbage88classification.top/alter_car?car_license=' + this.data.car_license,
        data: upData,
        method: "POST",
        header: {
          // POST+这种content-type，会将数据转换成 query string
          "content-type": "application/x-www-form-urlencoded",
        },
        success: function (res) {
          console.log(res)
          // 显示注册成功的提示框并自动跳转
          // 如果服务器返回的http状态码是SUCCESS_CODE,
          if (res.data.status==SUCCESS_CODE) {
            // 服务器通讯成功，并且返回的结果是通过：用黑色提示框来显示
            that.setData({
              succ_toast: true
            });
            setTimeout(() => {
                that.setData({
                    hide_succ_toast: true
                });
                setTimeout(() => {
                    that.setData({
                        succ_toast: false,
                        hide_succ_toast: false,
                    });
                    // 自动跳转到我的车辆页面       
                    wx.navigateTo({
                        url: '../dirverHomepage/dirverHomepage?user_id='+ res.data.user_id
                    });
                }, 300);
            }, 2000);
          }

          // 服务器通讯成功，但返回的结果是不通过：给出模态对话框提示
          else {
            switch(res.data){
              case F_BRAND:
                wx.showModal({
                  title: '提示',
                  content: '品牌不合法，请重新输入'
                });
                break;
              case F_COLOR:
                wx.showModal({
                  title: '提示',
                  content: '颜色不合法，请重新输入'
                });
                break;
              case F_PLATE_NUMBER:
                wx.showModal({
                  title: '提示',
                  content: '车牌号不合法，请重新输入'
                });
                break;
              default: break;
            }
          }
        },

        fail: function (e) {
          // 这里是因为服务器通讯失败，用黑色提示框来显示
          console.log('wrong');
          console.log(e);
          that.setData({
              warn_toast: true,
              msg: "修改失败：服务器通讯不通"
          });
          setTimeout(() => {
            that.setData({
                hide_warn_toast: true
            });
            setTimeout(() => {
                that.setData({
                    warn_toast: false,
                    hide_warn_toast: false,
                });
            }, 300);
          }, 2000);
        }
      })
      // //！！！！！！！！第二种上传方式（结束）
    }

    else if(!this.data.brand_ok){
        wx.showModal({
          title: '提示',
          content: '请重新输入品牌'
        })
    }
    else if(!this.data.color_ok){
      wx.showModal({
        title: '提示',
        content: '请重新输入颜色'
      })  
    }
    else if(!this.data.plateNumber_ok){
      wx.showModal({
        title: '提示',
        content: '请重新输入车牌号'
      })  
    } 
    else if(this.data.carPhoto==''){
      wx.showModal({
        title: '提示',
        content: '请上传车辆图片！'
      })  
    }
  },
})
