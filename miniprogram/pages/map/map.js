// pages/map/map.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        // 自己的位置
        self_latitude: 31.28664,
        self_longitude: 121.212065,
        // 标记点
        covers: [
            // 司机位置
            {
                latitude: 31.285400,
                longitude: 121.498543,
                iconPath: 'location.png'
            }
        ]
    },

    /**
     * 缩放视野，展示自己和司机的坐标点
     */
    moveToLocation: function () {
        this.mapCtx.includePoints({
            // 坐标点形成的矩形边缘到地图边缘的距离
            padding: [20],
            points: [
                // 自己位置
                {
                    latitude: this.data.self_latitude,
                    longitude: this.data.self_longitude,
                },
                // 司机位置
                {
                    latitude: this.data.covers[0].latitude,
                    longitude: this.data.covers[0].longitude
                }
            ]
        })
    },

    /**
     * 根据经纬度计算距离  距离单位为m  保留到个位
     */
    getDistance: function (la1, lo1, la2, lo2) {
        let La1 = la1 * Math.PI / 180.0;
        let La2 = la2 * Math.PI / 180.0;
        let La3 = La1 - La2;
        let Lb3 = lo1 * Math.PI / 180.0 - lo2 * Math.PI / 180.0;
        let s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(La3 / 2), 2) + Math.cos(La1) * Math.cos(La2) * Math.pow(Math.sin(Lb3 / 2), 2)));
        s = s * 6378.137;
        s = Math.round(s * 1000);
        return s;
    },


    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
        this.mapCtx = wx.createMapContext('myMap');
    },

    /**
     * 生命周期函数--监听页面显示
     * 开启位置定时刷新
     */
    onShow: function () {
        let that = this;
        // 更新位置的时间间隔
        const refresh_time_distance = 3000;
        this.refresh_loc = setInterval(() => {

            wx.getLocation({
                type: 'gcj02',
                success(res) {
                    // 更新司机位置、自己位置、自己和司机的距离
                    that.setData({
                        self_latitude: res.latitude,
                        self_longitude: res.longitude,
                        distance: that.getDistance(res.latitude,res.longitude, that.data.covers[0].latitude, that.data.covers[0].longitude)
                    });
                }
            });




            // 向服务器请求司机位置并在地图更新
            // wx.request({
            //     url: 'getDriverLoc',
            //     method: 'GET',
            //     success: function (res) {
            //         // 司机位置
            //         let driver_latitude = res.driver_latitude;
            //         let driver_logitude = res.driver_logitude;
            //         // 获取自己位置
            //         wx.getLocation({
            //             type: 'gcj02',
            //             success(res) {
            //                 // 更新司机位置、自己位置、自己和司机的距离
            //                 that.setData({
            //                     self_latitude: res.latitude,
            //                     self_longitude: res.longitude,
            //                     [`covers[0].latitude`]: driver_latitude,
            //                     [`covers[0].logitude`]: driver_logitude,
            //                     distance: that.getDistance(res.latitude,res.longitude, driver_latitude, driver_logitude)
            //                 });
            //             }
            //         });
            //     },
            //     fail:function() {
            //         wx.showToast({
            //           title: '定位失败',
            //           duration: 1000,
            //           icon: 'none'
            //         })
            //     }
            // });
        }, refresh_time_distance);

    },

    /**
     * 页面隐藏后关闭位置更新
     */
    onHide: function () {
        clearInterval(this.refresh_loc);
    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})