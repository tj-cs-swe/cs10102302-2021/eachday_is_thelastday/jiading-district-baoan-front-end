// miniprogram/pages/frequentOrder/frequentOrder.js
const SUCCESS_CODE = 1;
const FAIL_CODE = 0;
const routes = ['嘉定校区↔昌吉东路地铁站',
'嘉定校区↔上海汽车城地铁站',
'嘉定校区↔安亭地铁站',
'嘉定校区↔嘉定新城地铁站',
'嘉定校区↔虹桥火车站',
'嘉定校区↔虹桥机场', 
'嘉定校区↔四平校区',
'嘉定校区↔沪西校区',
'嘉定校区↔沪北校区']
const destinations = ['昌吉东路地铁站',
'上海汽车城地铁站',
'安亭地铁站',
'嘉定新城地铁站',
'虹桥火车站',
'虹桥机场', 
'四平校区',
'沪北校区',
'沪西校区']

Page({

  /**
   * 页面的初始数据
   */
  data: {
    routes,
    routeIndex: 3,
    source: '嘉定校区',
    destinations,
    destination: '嘉定新城地铁站',
    items: [
      { name: 'YES', value: '是' },
      { name: 'NO', value: '否', checked: 'true' },
    ],
    date: '2021-06-06',
    starttime: '00:00',
    endtime: '00:00',
    now: '00:00',
    today: '2021-06-06',
    start: '00:00'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var d = new Date;
    let _years = d.getFullYear();
    let _months = d.getMonth() + 1;
    _months = ( 10 >  _months) ? '0' + _months : _months;
    let _days = ( 10 > d.getDate() ) ? '0' + d.getDate() : d.getDate();
    let _hour = ( 10 > d.getHours() ) ? '0' + d.getHours() : d.getHours();
    let _minute = ( 10 > d.getMinutes() ) ? '0' + d.getMinutes() : d.getMinutes();
    this.setData({
      today: _years+'-'+_months+'-'+_days,
      date: _years+'-'+_months+'-'+_days,
      now: _hour+':'+_minute,
      starttime: _hour+':'+_minute,
      endtime: _hour+':'+_minute,
      start: _hour+':'+_minute,
      user_id: options.user_id
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  /**
   * 用户滑动picker
   */
  bindChange(e) {
    const index = e.detail.value
    console.log(e)
    this.setData({
      routeIndex: index,
      source: '嘉定校区',
      destination: destinations[index],
    })
  },

  /**
   * 用户切换出发地和目的地
   */
  onConvert: function (e) {
    this.setData({
      source: this.data.destination,
      destination: this.data.source,
    })
  },

  /**
   * 单选框勾选变化
   */
  radioChange: function (e) {
    console.log('radio发生change事件，携带value值为：', e.detail.value)
  },

  bindDateChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      date: e.detail.value,
    })
    if(this.data.date!=this.data.today){
      this.setData({
        start: '00:00',
      })
    }
    else{
      this.setData({
        start: now,
      })
    }
  },

  bindStartTimeChange: function (e) {
    console.log(this.data.start)
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      starttime: e.detail.value,
      endtime: e.detail.value,
    })
  },

  bindEndTimeChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      endtime: e.detail.value
    })
  },

  onOrder: function (e) {
    let that = this;
    console.log(this.data);
    wx.request({
      // url: 'https://garbage88classification.top/frequent_order',
      url: 'https://garbage88classification.top/passenger_call?user_id='+ this.data.user_id,
      data: this.data,
      method: "POST",
      header: {
        // POST+这种content-type，会将数据转换成 query string
        "content-type": "application/x-www-form-urlencoded",
      },
      success: function (res) {
        console.log(res)
        // 显示注册成功的提示框并自动跳转
        // 如果服务器返回的http状态码是SUCCESS_CODE,
        if (res.data==SUCCESS_CODE) {
          // 服务器通讯成功，并且返回的结果是通过
          setTimeout(() => {
              // 自动跳转到我的预约页面    
              wx.reLaunch({
                url: '../myAppointment/myAppointment?user_id=' + that.data.user_id
            });
          }, 300);
        }
        // 服务器通讯成功，但返回的结果是不通过
        else {
          if (res.data==0){
            setTimeout(() => {
              wx.showModal({
                title: '提示',
                content: '当前无空闲车辆，请您过会儿再来看看~'
              })  
            }, 300);
          }
        else if (res.data==2){
          setTimeout(() => {
            wx.showModal({
              title: '提示',
              content: '当前禁止预约，您已被禁止24小时'
            })  
          }, 300);
        }
        else{
          setTimeout(() => {
            wx.showModal({
              title: '提示',
              content: '当前禁止预约，您已被禁止7天'
            })  
          }, 300);
        }
        }
      },
      fail: function (e) {
        // 这里是因为服务器通讯失败，用黑色提示框来显示
        console.log('wrong');
        console.log(e);
      }
    })
  },
})
