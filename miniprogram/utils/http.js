var md5 = require('md5.js')
function req(url, data, cb, method) {

  for (var Key in data) {
    if (data[Key] === null) {
      delete data[Key];
    }
  }

  wx.request({
    url:  url,
    data: data,
    method: method || 'POST',
    header: {
      // POST+这种content-type，会将数据转换成 query string
      "content-type": "application/x-www-form-urlencoded",
      
    },
    success: function (res) {
      return typeof cb == "function" && cb(res)
    },
    fail: function () {
      return typeof cb == "function" && cb(false)
    }
  })
}
module.exports = {
  req: req,
}